import Vue from 'vue'
import App from './App.vue'
import router from './router'

//Importar componentes de Ionic
import Ionic from '@ionic/vue'
import '@ionic/core/css/ionic.bundle.css'
Vue.use(Ionic)

//Componentes de Font Awesome
import { library } from '@fortawesome/fontawesome-svg-core'
//Iconos a usar
import { faPlus,faTrashAlt,faStar,faHeart, faTimes, faBars, faCheck, faAdjust, faTasks,
faCog } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faPlus,faTrashAlt,faStar,faHeart,faTimes,faBars,faCheck,faAdjust,faTasks,faCog);

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
